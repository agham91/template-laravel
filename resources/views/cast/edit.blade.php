@extends('layout.master')

@section('judul')
    Halaman Cast
@endsection
@section('title')
<i class="fa fa-table" aria-hidden="true"></i>
<p>Templating Laravel  
<i class="right fas fa-angle-left"></i> </p>
@endsection

@section('title1')
Laravel CRUD
@endsection


@section('content')


<div>
    
        <form action="/cast/{{$cast->id,nama,umur,bio}}" method="POST">
            @method('put')
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                    <br>
                    <br>


                <label>umur</label><br>
                <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" placeholder="Masukkan umur anda">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror

                    <br>
                    <br>


                <label>bio</label><br>
                <textarea name="bio" value="{{$cast->bio}}" cols="30" rows="10"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div><br><br>
           
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection